module.exports = {
  development: {
    username: 'sharenote',
    password: 'sh4869',
    database: 'sharenote',
    host: process.env.DB_NAME || 'localhost',
    dialect: 'mysql'
  },
  test: {
    username: 'root',
    password: null,
    database: 'database_test',
    host: '127.0.0.1',
    dialect: 'mysql'
  },
  production: {
    username: 'root',
    password: null,
    database: 'database_production',
    host: '127.0.0.1',
    dialect: 'mysql'
  }
}
