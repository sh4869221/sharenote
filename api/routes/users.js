const { Router } = require('express')

const router = Router()
const models = require('../../models')

/* GET users listing. */
router.get('/users', function (req, res, next) {
  models.User.findAll().then(users => {
    res.json(users)
  })

  // res.json(
  //   users.map(user => {
  //     return user.get({ plain: true })
  //   }))
})

/* GET user by ID. */
router.get('/users/:id', function (req, res, next) {
  models.User.findByPk(req.params.id).then(user => {
    res.json(user)
  })
})

router.post('/users', function (req, res, next) {
  models.User.create()
})

router.get('/test', function (req, res, next) {
  res.json([{name: 'string', test: 5}, {name: 'test', test: 6}])
})

module.exports = router
