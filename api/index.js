const express = require('express')

// Create express instnace
const app = express()

// Require API routes
const users = require('./routes/users')

// Import API Routes
app.use(users)
app.set('models', require('../models'))

// Export the server middleware
module.exports = {
  path: '/api',
  handler: app
}
