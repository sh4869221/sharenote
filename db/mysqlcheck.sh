#!/bin/sh
# mysqlcheck.sh

set -e

host="$1"
shift
cmd="$@"

until mysqladmin ping -u$MYSQL_USER -h"$host" -p$MYSQL_PASSWORD; do
  >&2 echo "Mysql stop"
  sleep 1
done

>&2 echo "mysql is up - executing command"
exec $cmd
